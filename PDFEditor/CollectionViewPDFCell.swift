
//  CollectionViewPDFCell.swift
//  PDFEditor
//
//  Created by Vishal Vaghasiya on 10/04/19.
//  Copyright © 2019 Vishal Vaghasiya. All rights reserved.
//

import UIKit

class CollectionViewPDFCell: UICollectionViewCell {
    @IBOutlet var lblImg: UIImageView!
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var imgStatus: UIImageView!
}
