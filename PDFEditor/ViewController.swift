//
//  ViewController.swift
//  PDFEditor
//
//  Created by Vishal Vaghasiya on 10/04/19.
//  Copyright © 2019 Vishal Vaghasiya. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    fileprivate var data = ["Audio(H)","Audio(S)","Del Note(S)","Dyno(H)","Dyno(S)"]
    @IBOutlet var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
        let width = (collectionView.frame.width-20)/2
        layout.itemSize = CGSize(width: width, height: width)
        layout.minimumInteritemSpacing = 5
        collectionView!.collectionViewLayout = layout
    }
}

extension ViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewPDFCell
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.lblTitle.text = data[indexPath.row]
        if let pdf = Bundle.main.url(forResource: data[indexPath.row], withExtension: "pdf", subdirectory: nil, localization: nil)  {
            cell.lblImg.image = drawPDFfromURL(url: pdf)
        }
        return  cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell!.layer.borderWidth = 2.0
        cell!.layer.borderColor = UIColor.gray.cgColor
        
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "ViewPdf") as? ViewPdf
        vc?.urlPdf = Bundle.main.url(forResource: data[indexPath.row], withExtension: "pdf", subdirectory: nil, localization: nil)
        self.navigationController?.pushViewController(vc!, animated: false)
    }
}
extension ViewController{
    func drawPDFfromURL(url: URL) -> UIImage? {
       
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            ctx.cgContext.drawPDFPage(page)
        }
        return img
    }
}
extension UINavigationController{
    convenience init(_ rootvc:UIViewController){
        self.init(rootViewController: rootvc)
        navigationBar.barTintColor = #colorLiteral(red: 0.03137254902, green: 0.3096852005, blue: 0.6129576564, alpha: 1)
        navigationBar.tintColor = .white
    }
}
