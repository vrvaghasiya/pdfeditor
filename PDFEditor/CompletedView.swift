//
//  CompletedView.swift
//  PDFEditor
//
//  Created by Vishal Vaghasiya on 11/05/19.
//  Copyright © 2019 Vishal Vaghasiya. All rights reserved.
//

import UIKit
import PDFKit

class CompletedView: UIViewController {

    @IBOutlet var segment: UISegmentedControl!
    @IBOutlet var collectionView: UICollectionView!
    fileprivate var dicListPdf:[String:String]!
    fileprivate var listPdf:[String]!
    fileprivate var listPdfEditDate:[String]!
    let ud = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.title = "Completed"

        let str = segment.titleForSegment(at: segment.selectedSegmentIndex)
        dicListPdf = GetPDFList(type: str!)
        ReloadDate(dic: dicListPdf)
        
    }
    func ReloadDate(dic:Dictionary<String, String>)  {
        let Value = dicListPdf.values
        listPdf = Array(Value.map { String($0) })
        let Key = dicListPdf.keys
        let templistPdfEditDate = Array(Key.map { String($0) })
        
        var convertedArray: [Date] = []
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MM YYYY HHmmss"// yyyy-MM-dd"
        for dat in templistPdfEditDate {
            let date = dateFormatter.date(from: dat)
            if let date = date {
                convertedArray.append(date)
            }
        }
        
        listPdfEditDate = templistPdfEditDate.sorted(by: { $0.compare($1) == .orderedDescending })
        print(listPdfEditDate)
        collectionView.reloadData()
    }
    @IBAction func segmentClick(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        let str = segment.titleForSegment(at: sender.selectedSegmentIndex)
        dicListPdf = GetPDFList(type: str!)
        ReloadDate(dic: dicListPdf)
    }
}

extension CompletedView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listPdf.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewPDFCell
        
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        let key = listPdfEditDate[indexPath.row]
        let strURL = dicListPdf[key]
        
        let filePath = (documentsDirectory as NSString).appendingPathComponent(strURL!) as String
        cell.lblTitle.text = (URL(fileURLWithPath:strURL!).lastPathComponent as NSString).deletingPathExtension
        cell.lblDate.text = convertDateFormater(key)
        cell.lblImg.image = load(image: cell.lblTitle.text!, url: URL(fileURLWithPath: filePath))
        if isKeyPresentInUserDefaults(key: dicListPdf[key]!) {
            cell.imgStatus.isHidden=false
        }else{
            cell.imgStatus.isHidden=true
        }
        return  cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "ViewPdf") as? ViewPdf
        let key = listPdfEditDate[indexPath.row]

        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = (documentsDirectory as NSString).appendingPathComponent(dicListPdf[key]!) as String
        vc?.urlPdf = URL(fileURLWithPath:filePath)
        vc!.fileType = FileType(filename: dicListPdf[key]!)
        vc!.isEdit = true
        self.present(UINavigationController(vc!), animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let width = collectionView.bounds.width
        
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        let width:CGFloat
        let height:CGFloat
        
        switch (deviceIdiom) {
        case .pad:
            width = (collectionView.frame.size.width - 30.0) / 3.0
            height = width+100
        case .phone:
            width = (collectionView.frame.size.width - 10.0) / 2.0
            height = width+30
        default:
            width = (collectionView.frame.size.width - 30.0) / 3.0
            height = width+100
        }
        return CGSize(width: width, height: height)
    }
    func convertDateFormater(_ date: String) -> String
    {
        print(date)
        let inputFormatter = DateFormatter()
        inputFormatter.timeStyle = .short
        inputFormatter.dateStyle = .short
        let showDate = inputFormatter.date(from: date)
        print(showDate as Any)
        inputFormatter.dateFormat = "dd MMMM YYYY"
        let resultString = inputFormatter.string(from: showDate!)
        print(resultString)
        return resultString
    }
    func FileType(filename:String) -> String {
        var filetype:String!
        if filename.contains("Ref:"){
            filetype="Delivery Notes"
        }
        else{
            filetype="Certificates"
        }
        return filetype
    }
}
extension CompletedView{
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            ctx.cgContext.drawPDFPage(page)
        }
        return img
    }
    func generatePdfThumbnail(of thumbnailSize: CGSize , for documentUrl: URL, atPage pageIndex: Int) -> UIImage? {
        let pdfDocument = PDFDocument(url: documentUrl)
        let pdfDocumentPage = pdfDocument?.page(at: pageIndex)
        return pdfDocumentPage?.thumbnail(of: thumbnailSize, for: PDFDisplayBox.trimBox)
    }
    func GetPDFList(type:String) ->  Dictionary<String, String> {
        var list:[String]?=[]
        var listDate:[String]?=[]
        if type == "All"{
            var listDelivery:[String]?=[]
            var listDeliveryDate:[String]?=[]
            if isKeyPresentInUserDefaults(key: "Delivery Notes") {
                listDelivery = (ud.value(forKey: "Delivery Notes") as! [String])
                listDeliveryDate = (ud.value(forKey: "Delivery NotesDate") as! [String])
            }
            var listCertificates:[String]?=[]
            var listCertificatesDate:[String]?=[]

            if isKeyPresentInUserDefaults(key: "Certificates") {
                listCertificates = (ud.value(forKey: "Certificates") as! [String])
                listCertificatesDate = (ud.value(forKey: "CertificatesDate") as! [String])
            }
            list = listCertificates! + listDelivery!
            listDate = listCertificatesDate! + listDeliveryDate!
        }
        else if isKeyPresentInUserDefaults(key: type) {
            list = (ud.value(forKey: type) as! [String])
            listDate = (ud.value(forKey: "\(type)Date") as! [String])
        }
        let fullStack = Dictionary(uniqueKeysWithValues: zip(listDate!, list!))
        return fullStack
    }
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return ud.object(forKey: key) != nil
    }
    func load(image imageName: String, url:URL) -> UIImage {
        // declare image location
        let imagePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(imageName).png"
        let imageUrl: URL = URL(fileURLWithPath: imagePath)
        
        // check if the image is stored already
        if FileManager.default.fileExists(atPath: imagePath),
            let imageData: Data = try? Data(contentsOf: imageUrl),
            let image: UIImage = UIImage(data: imageData, scale: UIScreen.main.scale) {
            return image
        }
        
        // image has not been created yet: create it, store it, return it
        let thumbnailSize = CGSize(width: 300, height: 400)
        let newImage: UIImage = generatePdfThumbnail(of: thumbnailSize, for: url, atPage: 0)!
        try? newImage.pngData()?.write(to: imageUrl)
        return newImage
    }
}

extension String {
    static let shortDateUS: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateStyle = .short
        return formatter
    }()
    var shortDateUS: Date? {
        return String.shortDateUS.date(from: self)
    }
}
