//
//  DropboxFolderModel.swift
//  PDFEditor
//
//  Created by Vishal Vaghasiya on 15/05/19.
//  Copyright © 2019 Vishal Vaghasiya. All rights reserved.
//

import UIKit
import SwiftyJSON

class DropboxFolderModel: NSObject
{
    var entries: [Entries] = []
    
    init(_ json: JSON) {
        super.init()
        
        for vl in json["entries"].arrayValue {
            entries.append(Entries(vl))
        }
    }
}
class Entries: NSObject {
    var tag: String?
    var name: String?
    
    override init() {
        super.init()
    }
    
    init(_ json: JSON) {
        super.init()
        tag = json[".tag"].stringValue
        name = json["name"].stringValue
    }
}
