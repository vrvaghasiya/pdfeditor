//
//  SignView.swift
//  PDFEditor
//
//  Created by Vishal Vaghasiya on 11/04/19.
//  Copyright © 2019 Vishal Vaghasiya. All rights reserved.
//

import UIKit
protocol SignViewDelegate: class {
    func GetSignImage(img:UIImage)
}
class SignView: UIViewController,YPSignatureDelegate{
    weak var delegate: SignViewDelegate?
    @IBOutlet weak var signatureView: YPDrawSignatureView!

    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView.delegate = self
        signatureView.strokeWidth = 4
        let userBtn = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(AddSign_click))
        self.navigationItem.rightBarButtonItem = userBtn
    }
    @objc func AddSign_click() {
        
        if let signatureImage = self.signatureView.getSignature(scale: 10) {
            delegate?.GetSignImage(img: signatureImage)
        }
        
        navigationController?.popViewController(animated: true)
    }
    @IBAction func selectColor(_ sender: UIBarButtonItem) {
        if sender.tag == 1{
            signatureView.strokeColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }else if sender.tag == 2{
            signatureView.strokeColor = #colorLiteral(red: 0.01680417731, green: 0.1983509958, blue: 1, alpha: 1)
        }else{
            signatureView.strokeColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        }
    }
    @IBAction func Clear(_ sender: Any) {
        self.signatureView.clear()
    }
    
    func didStart(_ view : YPDrawSignatureView) {
        print("Started Drawing")
    }
    
    func didFinish(_ view : YPDrawSignatureView) {
        print("Finished Drawing")
    }

}
