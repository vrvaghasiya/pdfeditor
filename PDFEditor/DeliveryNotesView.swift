//
//  DeliveryNotesView.swift
//  PDFEditor
//
//  Created by Vishal Vaghasiya on 11/05/19.
//  Copyright © 2019 Vishal Vaghasiya. All rights reserved.
//

import UIKit
import PDFKit

class DeliveryNotesView: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    fileprivate var data = ["Del Note(S)"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.title = "Delivery Notes"
    }
}
extension DeliveryNotesView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewPDFCell
        if let pdf = Bundle.main.url(forResource: data[indexPath.row], withExtension: "pdf", subdirectory: nil, localization: nil)  {
            let thumbnailSize = CGSize(width: 300, height: 400)
            let thumbnail = generatePdfThumbnail(of: thumbnailSize, for: pdf, atPage: 0)
            cell.lblImg.image = thumbnail
        }
        cell.lblTitle.text = data[indexPath.row]

        return  cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "ViewPdf") as? ViewPdf
        vc?.urlPdf = Bundle.main.url(forResource: data[indexPath.row], withExtension: "pdf", subdirectory: nil, localization: nil)
        vc!.fileType = "Delivery Notes"
        self.present( UINavigationController(vc!), animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = collectionView.bounds.width
        
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        let width:CGFloat
        let height:CGFloat
        
        switch (deviceIdiom) {
        case .pad:
            width = (collectionView.frame.size.width - 30.0) / 3.0
            height = width+100
        case .phone:
            width = (collectionView.frame.size.width - 15.0) / 2.0
            height = width+30
        default:
            width = (collectionView.frame.size.width - 30.0) / 3.0
            height = width+100
        }
        return CGSize(width: width, height: height)
    }
}
extension DeliveryNotesView{
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            ctx.cgContext.drawPDFPage(page)
        }
        return img
    }
    func generatePdfThumbnail(of thumbnailSize: CGSize , for documentUrl: URL, atPage pageIndex: Int) -> UIImage? {
        let pdfDocument = PDFDocument(url: documentUrl)
        let pdfDocumentPage = pdfDocument?.page(at: pageIndex)
        return pdfDocumentPage?.thumbnail(of: thumbnailSize, for: PDFDisplayBox.trimBox)
    }
}
