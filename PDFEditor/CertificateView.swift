//
//  CertificateView.swift
//  PDFEditor
//
//  Created by Vishal Vaghasiya on 11/05/19.
//  Copyright © 2019 Vishal Vaghasiya. All rights reserved.
//

import UIKit
import PDFKit
import Kingfisher

class CertificateView: UIViewController {

    var textField: UITextField?
    @IBOutlet var collectionView: UICollectionView!
    fileprivate var data = ["Alco(S) NEW",
                            "ATU30(S)",
                            "Audio(S)",
                            "BAS(S)",
                            "Booth(S)",
                            "Calibrator (Updated)",
                            "CO(S)",
                            "CR 822B(S)",
                            "Defib(S)",
                            "Dyno (Baseline Hydraulic Pinch Gauge)",
                            "Dyno (Baseline Pinch Gauge)",
                            "Dyno (Camry)",
                            "Dyno (JAMAR Hydraulic Pinch Gauge)",
                            "Dyno (Sauter FA500)",
                            "Dyno (unknown)",
                            "Dyno Back",
                            "Dyno(S)",
                            "Easytymp - UPDATED (S)",
                            "ECG(S)",
                            "FonixFP35(S)",
                            "Gen(S)",
                            "GSITympstar(S)",
                            "Human Vibration Meter -",
                            "KLT25(S)",
                            "MT10(S)",
                            "Otowave102(S)",
                            "Parrot MTT(S)",
                            "Parrot Plus(S)",
                            "Physiotherapy",
                            "Scales(S)",
                            "Serv(S)",
                            "SLM(S)",
                            "SLMZ- A5 - UPDATED",
                            "Smokecheck(S)",
                            "Syringe(S)",
                            "Takei Back(S)",
                            "TSM300(S)",
                            "Warbler(S)"]
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.title = "Certificates"
    }
}
extension CertificateView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewPDFCell
        if let pdf = Bundle.main.url(forResource: data[indexPath.row], withExtension: "pdf", subdirectory: nil, localization: nil)  {
            cell.lblImg.image = load(image: data[indexPath.row], url: pdf)
        }
        cell.lblTitle.text = data[indexPath.row]
        return  cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: "New Certificate", message: "Please enter the number for this certificate", preferredStyle: .alert)
        alert.addTextField(configurationHandler: configurationTextField)

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:nil))
        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {_ in
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "ViewPdf") as? ViewPdf
            vc?.urlPdf = Bundle.main.url(forResource: self.data[indexPath.row], withExtension: "pdf", subdirectory: nil, localization: nil)
            vc!.strSerielNo = self.textField?.text
            vc!.fileType = "Certificates"
            self.present( UINavigationController(vc!), animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func configurationTextField(textField: UITextField!) {
        if (textField) != nil {
            self.textField = textField!        //Save reference to the UITextField
            self.textField?.placeholder = "Serial number";
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let deviceIdiom = UIScreen.main.traitCollection.userInterfaceIdiom
        let width:CGFloat
        let height:CGFloat
        
        switch (deviceIdiom) {
        case .pad:
            width = (collectionView.frame.size.width - 30.0) / 3.0
            height = width+100
        case .phone:
            width = (collectionView.frame.size.width - 15.0) / 2.0
            height = width+30
        default:
            width = (collectionView.frame.size.width - 30.0) / 3.0
            height = width+100
        }
        return CGSize(width: width, height: height)
    }
}
extension CertificateView{
    func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            ctx.cgContext.drawPDFPage(page)
        }
        return img
    }
    func generatePdfThumbnail(of thumbnailSize: CGSize , for documentUrl: URL, atPage pageIndex: Int) -> UIImage? {
        let pdfDocument = PDFDocument(url: documentUrl)
        let pdfDocumentPage = pdfDocument?.page(at: pageIndex)
        return pdfDocumentPage?.thumbnail(of: thumbnailSize, for: PDFDisplayBox.artBox)
    }
    func load(image imageName: String, url:URL) -> UIImage {
        // declare image location
        let imagePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(imageName).png"
        let imageUrl: URL = URL(fileURLWithPath: imagePath)
        
        // check if the image is stored already
        if FileManager.default.fileExists(atPath: imagePath),
            let imageData: Data = try? Data(contentsOf: imageUrl),
            let image: UIImage = UIImage(data: imageData, scale: UIScreen.main.scale) {
            return image
        }
        
        // image has not been created yet: create it, store it, return it
        let thumbnailSize = CGSize(width: 300, height: 400)
        let newImage: UIImage = generatePdfThumbnail(of: thumbnailSize, for: url, atPage: 0)!
        try? newImage.pngData()?.write(to: imageUrl)
        return newImage
    }
}
