//
//  ViewPdf.swift
//  PDFEditor
//
//  Created by Vishal Vaghasiya on 11/04/19.
//  Copyright © 2019 Vishal Vaghasiya. All rights reserved.
//

import UIKit
import PDFKit
import MessageUI
import SwiftyDropbox
import MBProgressHUD
import SwiftyJSON




class ViewPdf: UIViewController,SignViewDelegate,MFMailComposeViewControllerDelegate {
   
    @IBOutlet weak var pdfContainerView: PDFView!
    
    var urlPdf:URL!
    var strSerielNo:String!
    var currentlySelectedAnnotation: PDFAnnotation?
    var fileType:String!
    let ud = UserDefaults.standard
    var isEdit:Bool!

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(UploadFile),
                                               name: Notification.MyApp.Something,
                                               object: nil)

        navigationController?.navigationItem.hidesBackButton = true
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        //Left Barbutton icon
        let newBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(Done_click))
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationItem.leftBarButtonItem = newBtn
        
        //Right Barbutton icon
        let userBtn = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(Share_click))
        self.navigationItem.rightBarButtonItem = userBtn
        setupPdfView()
        
        let panAnnotationGesture = UIPanGestureRecognizer(target: self, action: #selector(didPanAnnotation(sender:)))
        pdfContainerView.addGestureRecognizer(panAnnotationGesture)    
    }
    override func viewWillAppear(_ animated: Bool) {
       
      /*  let filename = self.urlPdf.lastPathComponent as String
        if isEdit == true {
            if isKeyPresentInUserDefaults(key: filename) {
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }else{
                self.navigationItem.rightBarButtonItem?.isEnabled = true
            }
        }
 */
    }
    @IBAction func AddSign(_ sender: Any) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "SignView") as? SignView
        vc!.delegate=self
        navigationController?.pushViewController(vc!, animated: false)
    }
    @IBAction func AddText(_ sender: Any) {
    }
    @objc func Done_click() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func Share_click() {
        let filename:String!
        let documento:NSData!
        // Create file name
        if isEdit == true {
            filename = self.urlPdf.lastPathComponent as String
            self.pdfContainerView.document?.write(toFile: self.urlPdf.path)
            documento = NSData(contentsOfFile: self.urlPdf.path)
            
        }else{
            filename = self.CreateFileName(url: self.urlPdf,type:self.fileType)
            let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let filePath = (documentsDirectory as NSString).appendingPathComponent(filename) as String
            self.pdfContainerView.document?.write(toFile: filePath)
            documento = NSData(contentsOfFile: filePath)
            self.SavePDF(pdfName: filename, type: self.fileType, EditTime: self.getCurrentTime()!)
        }
        
        
        // Check dropbox login..
        
        if let client = DropboxClientsManager.authorizedClient{
            
            DispatchQueue.main.async {
                
                // Progress bar
                let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                hud.mode = .annularDeterminate
                hud.label.text = "File uploading on dropbox."
             
                
//                upload(path: "/\(self.fileType!)/\(filename!)", mode: .overwrite, autorename: false, clientModified: nil, mute: false, input: description).response { response, error in
                
                client.files.upload(path: "/\(self.fileType!)/\(filename!)", mode: .overwrite, autorename: false, clientModified:nil, mute: false, propertyGroups: nil, strictConflict: false, input: documento! as Data) .response{ asd, err in
                    if let response = asd {
                        print(response)
                        hud.hide(animated: true)
                        
                        self.ud.set(true, forKey: filename)
                        self.ud.synchronize()
                        
                        let alert = UIAlertController(title: "File uploaded", message:nil, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {_ in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }else if let error = err {
                        print(error)
                        self.dismiss(animated: true, completion: nil)
                        print(error)
                        hud.hide(animated: true)
                    }
                    }.progress({ (Progress) in
                        print(Progress.fractionCompleted)
                        hud.progress = Float(Progress.fractionCompleted)
                    })
            
                
               /*
                // Upload on dropbox
                client.files!.upload(path: "/\(self.fileType!)/\(filename!)", input: documento! as Data)
                    .response { response, error in
                        
                        if let response = response {
                            print(response)
                            hud.hide(animated: true)
                            
                            self.ud.set(true, forKey: filename)
                            self.ud.synchronize()
                            
                            let alert = UIAlertController(title: "File uploaded", message:nil, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {_ in
                                self.dismiss(animated: true, completion: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                        } else if let error = error {
                            self.dismiss(animated: true, completion: nil)
                            print(error)
                            hud.hide(animated: true)
                        }
                    }
                    .progress { progressData in
                        print(progressData.fractionCompleted)
                        hud.progress = Float(progressData.fractionCompleted)
 
 
                }
                */
            }
          
        } else {
            myButtonInControllerPressed()
        }
    }
    @objc func UploadFile(){
        
        DispatchQueue.main.async {
            
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = .annularDeterminate
            hud.label.text = "File uploading on dropbox."
            let filename = self.CreateFileName(url: self.urlPdf,type:self.fileType)
            let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let filePath = (documentsDirectory as NSString).appendingPathComponent(filename) as String
            
            self.pdfContainerView.document?.write(toFile: filePath)
            let documento = NSData(contentsOfFile: filePath)
            
            let client = DropboxClientsManager.authorizedClient
            
            client!.files!.upload(path: "/\(self.fileType!)/\(filename)", input: documento! as Data)
                .response { response, error in
                    if let response = response {
                        print(response)
                        hud.hide(animated: true)
                        self.ud.set(true, forKey: filename)
                        self.ud.synchronize()
                        let alert = UIAlertController(title: "File uploaded", message:nil, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: {_ in
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    } else if let error = error {
                        print(error)
                        hud.hide(animated: true)
                    }
                }
                .progress { progressData in
                    print(progressData.fractionCompleted)
                    hud.progress = Float(progressData.fractionCompleted)
            }
        }
    }
    
    func setupPdfView() {
        print(urlPdf)
        let document = PDFDocument(url: urlPdf)
        pdfContainerView.document = document
        pdfContainerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pdfContainerView.autoScales = true
        
        guard (strSerielNo != nil) else {
            return
        }
      
        let page = document?.page(at: 0)
        let size = page?.annotations[0].value(forAnnotationKey: PDFAnnotationKey(rawValue: "Rect")) as! CGRect
        let textField = PDFAnnotation(bounds: CGRect(x: size.origin.x, y: size.origin.y, width: size.width, height: size.height),
                                      forType: .widget,
                                      withProperties: nil)
        textField.widgetFieldType = .text
        textField.backgroundColor = .clear
        textField.font = UIFont.systemFont(ofSize: 14.0)
        textField.widgetStringValue = strSerielNo
        page!.addAnnotation(textField)
        page?.removeAnnotation((page?.annotations[0])!)
    }
    
    // MARK: - Alert
    func Alert(str:String) {
        let alert = UIAlertController(title: str, message:nil, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func myButtonInControllerPressed() {
        DropboxClientsManager.authorizeFromController(UIApplication.shared,
                                                      controller: self,
                                                      openURL: { (url: URL) -> Void in
                                                        print("success")
                                                       
                                                        UIApplication.shared.open(url, options: [:], completionHandler:{
                                                            (success) in
                                                            print("Open)")
                                                        })
                                                    
        })
  }
    func CreateFileName(url:URL,type:String) -> String {
        
        var strFileName:String
        if type == "Delivery Notes" {
            strFileName = "Ref:"   // File name without extension
        }else{
            strFileName = (url.lastPathComponent as NSString).deletingPathExtension   // File name without extension
        }
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMYYHHmmss"
        let result = formatter.string(from: date)
        strFileName = "\(strFileName)\(result).pdf"
        return strFileName
    }
    func SavePDF(pdfName:String,type:String,EditTime:String){
        
        var listPDF:[String]=[]
        var listPDFCreateDate:[String]=[]
        if isKeyPresentInUserDefaults(key: type) {
            listPDF = (ud.value(forKey: type) as! [String])
            listPDFCreateDate = (ud.value(forKey: "\(type)Date") as! [String])
        }
        listPDF.append(pdfName)
        listPDFCreateDate.append(EditTime)
        
        print(listPDF as Any)
        print(listPDFCreateDate as Any)
        
        ud.set(listPDF, forKey: type)
        ud.set(listPDFCreateDate, forKey: "\(type)Date")
    
        
        ud.synchronize()
        
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return ud.object(forKey: key) != nil
    }
    func getCurrentTime() -> String? {
        let date = Date()
        let formatter = DateFormatter()

//        let result = formatter.string(from: date)
        
        formatter.timeStyle = .short
        formatter.dateStyle = .short
        let result = formatter.string(from: date)
        print(result)

       return result
    }
    func GetSignImage(img: UIImage) {
        let page = pdfContainerView.currentPage
        let imageBounds = CGRect(x: 0, y:20, width: 200, height: 100)
        let imageStamp = ImageStampAnnotation(with: img, forBounds: imageBounds, withProperties: nil)
        page!.addAnnotation(imageStamp)
    }
    
    @objc func draggedView(_ sender:UIPanGestureRecognizer){
        let translation = sender.translation(in: pdfContainerView)
        sender.view!.center = CGPoint(x: sender.view!.center.x + translation.x, y: sender.view!.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @objc func didPanAnnotation(sender: UIPanGestureRecognizer) {
        let touchLocation = sender.location(in: pdfContainerView)
        
        guard let page = pdfContainerView.page(for: touchLocation, nearest: true)
            else {
                return
        }
        let locationOnPage = pdfContainerView.convert(touchLocation, to: page)
        
        switch sender.state {
        case .began:
            
            guard let annotation = page.annotation(at: locationOnPage) else {
                return
            }
            
            if annotation.isKind(of: ImageStampAnnotation.self) {
                currentlySelectedAnnotation = annotation
            }
            
        case .changed:
            
            guard let annotation = currentlySelectedAnnotation else {
                return
            }
            let initialBounds = annotation.bounds
            // Set the center of the annotation to the spot of our finger
            annotation.bounds = CGRect(x: locationOnPage.x - (initialBounds.width / 2), y: locationOnPage.y - (initialBounds.height / 2), width: initialBounds.width, height: initialBounds.height)
            
            
        //            print("move to \(locationOnPage)")
        case .ended, .cancelled, .failed:
            currentlySelectedAnnotation = nil
        default:
            break
        }
    }
}



public extension Notification {
    public class MyApp {
        public static let Something = Notification.Name("Notification.MyApp.Something")
    }
}
class ImageStampAnnotation: PDFAnnotation {
    
    var image: UIImage!
    
    // A custom init that sets the type to Stamp on default and assigns our Image variable
    init(with image: UIImage!, forBounds bounds: CGRect, withProperties properties: [AnyHashable : Any]?) {
        super.init(bounds: bounds, forType: PDFAnnotationSubtype.stamp, withProperties: properties)
        self.image = image
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func draw(with box: PDFDisplayBox, in context: CGContext) {
        // Get the CGImage of our image
        guard let cgImage = self.image.cgImage else { return }
        
        // Draw our CGImage in the context of our PDFAnnotation bounds
        context.draw(cgImage, in: self.bounds)
    }
}
